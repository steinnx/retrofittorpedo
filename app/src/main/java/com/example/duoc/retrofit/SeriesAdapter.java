package com.example.duoc.retrofit;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.duoc.retrofit.retrofit.modelos.Series;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by DUOC on 07-07-2017.
 */

public class SeriesAdapter extends RecyclerView.Adapter<SeriesAdapter.SeriesViewHolder>  {

    private ArrayList<Series> dataSource;
    private Context context;

    public SeriesAdapter(Context context) {
        this.context = context;
        dataSource= new ArrayList<>();
    }

    @Override
    public SeriesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_serie,parent,false);


        return new SeriesViewHolder(view);
    }

    @Override
    public void onBindViewHolder(SeriesViewHolder holder, int position) {
        final Series s = dataSource.get(position);
        holder.tv_nombre.setText(s.getTitle());
        holder.tv_año.setText(s.getYear());
        holder.tv_genero.setText(s.getGenres().getClass().toString());
        holder.tv_num_temporadas.setText("Numero Temporadas: "+String.valueOf(s.getNumSeasons()));
        holder.tv_rating.setText("Rating"+s.getRatingSerie().getPercentage());
        Picasso.with(context).load(s.getPoster().getBanner()).into(holder.iv_foto);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(context, DetalleActivity.class);
                i.putExtra("foto",s.getPoster().getBanner());
                i.putExtra("nombre",s.getTitle());
                i.putExtra("año",s.getYear());
                context.startActivity(i);
            }
        });


    }

    @Override
    public int getItemCount() {
        return dataSource.size();
    }

    public void adiccionarLista(List<Series> series) {
        dataSource.addAll(series);
        notifyDataSetChanged();
    }

    public class  SeriesViewHolder extends RecyclerView.ViewHolder{

        private TextView tv_nombre,tv_año,tv_genero,tv_rating,tv_num_temporadas;
        private ImageView iv_foto;

        public SeriesViewHolder(View itemView) {
            super(itemView);
            tv_nombre = (TextView) itemView.findViewById(R.id.tv_nombre_Serie);
            tv_año = (TextView) itemView.findViewById(R.id.tv_año);
            tv_genero = (TextView) itemView.findViewById(R.id.tv_genero);
            tv_rating = (TextView) itemView.findViewById(R.id.tv_rating);
            tv_num_temporadas = (TextView) itemView.findViewById(R.id.tv_num_tempora);
            iv_foto = (ImageView) itemView.findViewById(R.id.iv_foto);

        }
    }

}
