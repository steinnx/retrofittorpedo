package com.example.duoc.retrofit;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.duoc.retrofit.retrofit.ApiEndPointInterface;
import com.example.duoc.retrofit.retrofit.modelos.Series;
import com.example.duoc.retrofit.retrofit.modelos.SeriesList;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ListarFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 */
public class ListarFragment extends Fragment {

    Retrofit refrofit;
    RecyclerView recyclerView;
    SeriesAdapter seriesAdapter;


    private OnFragmentInteractionListener mListener;

    public ListarFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_listar, container, false);
        seriesAdapter = new SeriesAdapter(getContext());
        recyclerView = (RecyclerView) view.findViewById(R.id.rv_listar);
        recyclerView.setAdapter(seriesAdapter);
        recyclerView.setHasFixedSize(true);
        final GridLayoutManager gridLayoutManager= new GridLayoutManager(getContext(),1);
        recyclerView.setLayoutManager(gridLayoutManager);


        refrofit= new Retrofit.Builder()
                .baseUrl("https://api.myjson.com/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        obtenerDatos();



        /*recyclerView.addOnItemTouchListener(
                new RecyclerItemClickListener(getContext(), recyclerView ,new RecyclerItemClickListener.OnItemClickListener() {
                    @Override public void onItemClick(View view, int position) {
                        Intent i = new Intent(getContext(), DetalleActivity.class);
                        TextView v_nombre= (TextView) view.findViewById(R.id.tv_nombre_Serie);
                        String v_nom= v_nombre.getText().toString();
                        i.putExtra("nombre",v_nom);
                        getContext().startActivity(i);                    }

                    @Override public void onLongItemClick(View view, int position) {
                        // do whatever
                    }
                })
        );*/


        return view;


    }

    private void obtenerDatos() {
        ApiEndPointInterface anInterface = refrofit.create(ApiEndPointInterface.class);
        Call<SeriesList> seriesListCall = anInterface.getListAnime();
        seriesListCall.enqueue(new Callback<SeriesList>() {
            @Override
            public void onResponse(Call<SeriesList> call, Response<SeriesList> response) {
                if (response.isSuccessful()){
                    SeriesList seriesList = response.body();
                    List<Series> series = seriesList.getList();
                    seriesAdapter.adiccionarLista(series);
                }else {
                    Log.e("ERRORRRRR!!!!!!!!!!!!!!","onResponeseeeeeeeeeeee: "+response.errorBody());
                }
            }

            @Override
            public void onFailure(Call<SeriesList> call, Throwable t) {
                Log.e("ERRORRRRR!!!!!!!!!!!!!!","onFailureeeeeeeeeee: "+t.getMessage());
            }
        });


    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
