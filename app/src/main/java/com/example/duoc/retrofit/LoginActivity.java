package com.example.duoc.retrofit;

import android.net.Uri;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class LoginActivity extends AppCompatActivity implements ListarFragment.OnFragmentInteractionListener{

    Button btn_listar;
    Toolbar tolbar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        tolbar= (Toolbar)findViewById(R.id.toolbar);
        tolbar.setTitle("Jorge Reyes");
        setSupportActionBar(tolbar);

        btn_listar = (Button) findViewById(R.id.btn_listar);

        btn_listar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addFragment(new ListarFragment());
            }
        });
    }
    private void addFragment(Fragment fragment){
        if (null != fragment){
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.mi_frame, fragment)
                    .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                    .addToBackStack(null)
                    .commit();
        }
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }
}
