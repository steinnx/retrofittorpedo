package com.example.duoc.retrofit.retrofit;


import com.example.duoc.retrofit.retrofit.modelos.SeriesList;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;


import static com.example.duoc.retrofit.retrofit.Constants.APIConstants.ANIME_LIST;

/**
 * Created by hardroidlabs on 18-05-17.
 */

public interface ApiEndPointInterface {
    @GET(ANIME_LIST)
    Call<SeriesList> getListAnime();

}
