package com.example.duoc.retrofit;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

public class DetalleActivity extends AppCompatActivity {

    TextView tv_nombre_detalle,tv_año_detalle;
    ImageView iv_foto;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalle);
        Bundle extras = getIntent().getExtras();
        String nom= extras.getString("nombre");
        String año= extras.getString("año");
        String foto= extras.getString("foto");
        tv_nombre_detalle = (TextView) findViewById(R.id.v_titulo_detalle);
        tv_año_detalle = (TextView) findViewById(R.id.v_año);
        iv_foto = (ImageView) findViewById(R.id.iv_imagen_detalle);
        tv_nombre_detalle.setText(nom);
        tv_año_detalle.setText(año);
        Picasso.with(this).load(foto).into(iv_foto);


    }
}
